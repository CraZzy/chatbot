import './index.scss';

const ChatBot = class {
  constructor(bots) {
    this.el = document.querySelector('#app');
    this.bots = bots;
  }

  renderHeader() {
    return `
        <header>
            <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-primary p-4">
                <h4 class="text-white">Collapsed content</h4>
                <span class="text-muted">Toggleable via the navbar brand.</span>
                </div>
            </div>
            <nav class="navbar navbar-dark bg-primary">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <span class="navbar-brand mb-0 h1">Projet Chatbox Alexis</span>
            </nav>
            </div>
        </header>
        `;
  }

  renderContainer() {
    return `
        <main class="container-fluid " id="mainPage">
            <div class="row">
            ${this.renderBotsLists()}
            ${this.renderContentMessages()}
            </div>
        </main>
    `;
  }

  renderContentMessages() {
    return `
    <section class="col-9 pl-8 contentMessages">
        <div class="row inputMessage">
          <div class="form-group col-11">
            <input class="form-control" type="text" placeholder="Écrivez quelque chose">
          </div>
          <div class="col-1">
            <button type="submit" class="btn btn-primary">Send</button>
          </div>
        </div>
        <div class="row">
        ${this.renderMessagesSent()}
        ${this.renderMessagesReceived()}
          </div>
        </div>       
      </section>
    `;
  }

  renderMessagesSent() {
    return `
    <div class="row">
      <div class="col-8"></div>
      <div class="col-4">
        <div class="card text-white bg-primary mb-3" style="max-width: 24rem;">
          <div class="card-body">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  renderMessagesReceived() {
    return `
        <div class="row p-3">
        <div class="col-1 pt-2 text-center">
        <img src="https://png.pngitem.com/pimgs/s/146-1468281_profile-icon-png-transparent-profile-picture-icon-png.png" class="img-fluid rounded ml-r" width="64" alt="avatar">            </div>
        <div class="col-4">
        <div class="card text-dark bg-light mb-3" style="max-width: 24rem;">
            <div class="card-body">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>
        <div class="dateEtHeureMessages">Wen, 22 Dec 2021 21:12 GMT</div>
        </div>
        </div>
    `;
  }

  renderBotsLists() {
    return `
        <section class="col-3" id="ContactList">
        ${this.bots.map((bot) => this.renderBot(bot)).join('')}    ${this.renderBot()}
        </section>
    `;
  }

  renderBot() {
    return `
    <div class="row">
    <div class="col-2">
      <img src="https://png.pngitem.com/pimgs/s/146-1468281_profile-icon-png-transparent-profile-picture-icon-png.png" class="img-fluid rounded ml-r" width="64" alt="avatar">
    </div>
    <div class="col-4 pt-3">BOT 2</div>
    <div class="col-4 pt-3 ml-auto">
      <span class="badge bg-primary rounded-pill">1</span>
    </div>
    </div>
    <hr class="col-9">
    `;
  }

  run() {
    this.el.innerHTML += this.renderHeader();
    this.el.innerHTML += this.renderContainer();
  }
};

const bots = [{
  name: 'Georges',
  avatar: 'https://png.pngitem.com/pimgs/s/146-1468281_profile-icon-png-transparent-profile-picture-icon-png.png'
}, {
  name: 'Karim',
  avatar: 'https://png.pngitem.com/pimgs/s/146-1468281_profile-icon-png-transparent-profile-picture-icon-png.png'
}];

const chatbot = new ChatBot(bots);

chatbot.run();
